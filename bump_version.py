#! /usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import subprocess
from pathlib import Path
from datetime import datetime
from tempfile import TemporaryDirectory

# run date
NOW = datetime.now()

# string format for datetime
TIME_OUT_STRFORMAT = '%Y%m%dT%H%M%S'

# Current dir.
CWD = Path(__file__).parent.absolute()

# CHANGELOG file (by default)
CHANGELOG_FILE = CWD / 'CHANGELOG.md'

# pyproject file
PYPROJECT_FILE = CWD / 'pyproject.toml'


def get_version(changelog):
    import re
    """Get latest version from the input CHANGELOG.md file."""
    pattern = re.compile(r'(\d*)\.(\w*)\.(\w*)')
    if changelog.is_file():
        with changelog.open('rt') as file:
            for line in file:
                if pattern.match(line):
                    return line.strip()

    print(f'WARNING: {changelog} not found or invalid, version get be returned!')
    return 'unknown'


def main():

    parser = argparse.ArgumentParser(description='Set version and modification message '
                                                 'in CHANGELOG.md file (version is retrieved from pyproject.toml file)')
    parser.add_argument('-m', '--modifications',
                        required=True,
                        nargs='*',
                        type=str,
                        default=[None],
                        help='List of changes to save in CHANGELOG.md. '
                             'In case of multiple changes, the items must be separated by a semi-colon ";" character')
    parser.add_argument('-c', '--changelog',
                        nargs=1,
                        type=Path,
                        default=[CHANGELOG_FILE],
                        help=f'CHANGELOG file path. '
                             f'Default is {CHANGELOG_FILE}.')
    parser.add_argument('-p', '--pyproject',
                        nargs=1,
                        type=Path,
                        default=[PYPROJECT_FILE],
                        help=f'pyproject.toml file path. '
                             f'Default is {PYPROJECT_FILE}.')
    parser.add_argument('--force',
                        action='store_true',
                        default=False,
                        help='Force bump'
                        )
    args = parser.parse_args()
    changelog = args.changelog[0]
    pyproject_path = args.pyproject[0]
    modifications = ' '.join(args.modifications)

    import toml

    root_dir = Path(__file__).parent

    # Get version from pyproject.toml
    pyproject_data = toml.load(pyproject_path)
    version = pyproject_data['tool']['poetry']['version']

    print(f'Updating {changelog} with version "{version}" and modifications: "{modifications}" ...')

    # First build the version message to insert into CHANGELOG file
    # (including break lines)
    message = ['']
    message.append(version)
    message.append('-' * len(version))
    for current_item in modifications.split(';'):
        message.append(f'- {current_item.strip()}')

    # Open current changelog file
    with open(changelog, 'r') as input_file:
        metadata = input_file.readlines()

    if f'{version}\n' in metadata:
        print(f'{version} already found in {changelog}')
        if not args.force:
            return

    # Add new message at the top of the changelog file (but after title)
    title_offset = 2
    for i, current_line in enumerate(message):
        metadata.insert(title_offset + i, current_line + '\n')

    # Write into new changelog file
    with open(changelog, 'w') as output_file:
        for current_line in metadata:
            output_file.write(current_line)

    print('Done')


if __name__ == '__main__':
    main()
