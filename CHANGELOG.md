CHANGELOG
====================

1.2.0
-----
- Update license (CeCILL 2.1)
- Update Python dev dependencies in pyproject.toml

1.1.1
-----
- installation env update

1.1.0
------
- Add clear() method to Singleton class
- Add unit tests in tests/

1.0.1
-----
- Update pyproject.toml and poetry.lock

1.0.0
-----
- Update spiceypy to 5.0.1

0.5.0
-----
- Add cuc2str and cuc2datetime methods for SpiceManager

0.4.2
------
- Change LICENSE
- Add obt2utc and utc2obt methods to SpiceManager class

0.4.1
-----
- Update setup env

0.4.0
-----
* Introduce PEP518 installation mechanism

0.3.0
-----
* Add kall() method

0.2.2
-----
* Fix bug in SpiceManager.unload()
* Add _spiceypy attribute to SpiceManager class

0.2.0
-----
* Add time conversion methods

0.1.2
-----
* Fix syntax error in requirements.txt

0.1.1
-----
* Update setup.py to change requirements.txt loading

0.1.0
-----
* First release
