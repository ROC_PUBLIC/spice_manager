spice_manager
==============

Python Package to handle SPICE kernels.

## Quickstart

To install package using [pip](https://pypi.org/project/pip-tools/):

```
pip install spice_manager
```

## User guide

Package modules can be then imported into codes with line `import spice_manager`.

## Authors

- xavier dot bonnin at obspm dot fr
