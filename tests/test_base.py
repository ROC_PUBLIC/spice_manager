#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Contains test method/class baseline.
"""

from spice_manager import SpiceManager

from .constants import TEST_DATA_PATH

# create a decorator to load SPICE kernels with SpiceManager faster
def init_spice_manager(func):
    def wrapper_func():
        # Call function with a clean instance of SpiceManager
        SpiceManager.clear()
        spice = SpiceManager()
        # Load kernels
        spice.kernels = TEST_DATA_PATH
        # Run function
        func(spice)
        # Clear loaded kernels after call and clear SpiceManager again
        spice.unload_all()
        SpiceManager.clear()
    return wrapper_func
